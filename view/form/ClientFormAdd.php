<div id="content">
    <form method="post" action="">
        <fieldset>
            <legend>Add client</legend>
            <label>Id *:</label>
            <input type="text" placeholder="Id" name="id" value="<?php if (isset($content)) { echo $content->getId(); } ?>" />
            <label>Name *:</label>
            <input type="text" placeholder="Name" name="name" value="<?php if (isset($content)) { echo $content->getName(); } ?>" />
            <label>Surname :</label>
            <textarea  name="surname" placeholder="Surname" rows="10" cols="30" style="width: 236px; height: 40px; margin-left: 17px; border-radius: 4px;"><?php if (isset($content)) { echo $content->getSurname(); } ?></textarea>
            <label>Product :</label>
             <select name="product">
                    <option value="default">------------</option>
                      <?php
                      
                           foreach($products as $product){
                               
                                 if (isset($content) && ($content->getProduct() == $product->getId())) {
                                       echo '<option value="'.$product->getId().'" selected >'.$product->getName().'</option>';
                                        //print_r("content form add".$product->getId());
                                   }else{
                                       echo '<option value="'.$product->getId().'">'.$product->getName().'</option>';
                                        //print_r("content form add 2".$product->getId());
                                   }
                     
                           }  
                                 
                      ?>
                  
            </select>
                
            <label>* Required fields</label>
            <input type="submit" name="action" value="add" />
            <input type="submit" name="reset" value="reset" onClick="form_reset(this.form.id); return FALSE;" />
        </fieldset>
    </form>
</div>