<div id="content">
    <fieldset>
        <legend>Client list</legend>    
        <?php
            if (isset($content)) {
                echo <<<EOT
                    <table>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Surname</th>
                            <th>Product</th>
                           
                        </tr>
EOT;
                foreach ($content as $client) {
                    echo <<<EOT
                        <tr>
                            <td>{$client->getId()}</td>
                            <td>{$client->getName()}</td>
                            <td>{$client->getSurname()}</td>
                            <td>{$client->getProduct()}</td>
                            
                        </tr>
EOT;
                }
                echo <<<EOT
                    </table>
EOT;
            }
        ?>
    </fieldset>
</div>
