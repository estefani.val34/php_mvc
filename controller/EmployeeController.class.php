<?php

require_once "controller/ControllerInterface.php";
require_once "view/EmployeeView.class.php";
require_once "model/EmployeeModel.class.php";
require_once "model/Employee.class.php";
require_once "util/EmployeeMessage.class.php";
require_once "util/EmployeeFormValidation.class.php";

class EmployeeController implements ControllerInterface {

    private $view;
    private $model;

    public function __construct() {
        // carga la vista
        $this->view = new EmployeeView();
        
        // carga el modelo de datos
        $this->model = new EmployeeModel();
    }

    // carga la vista según la opción o ejecuta una acción específica
    public function processRequest() {

        $request = NULL;
        $_SESSION['info'] = array();
        $_SESSION['error'] = array();

        // recupera la acción de un formulario
        if (filter_has_var(INPUT_POST, 'action')) {
            $request = filter_has_var(INPUT_POST, 'action') ? filter_input(INPUT_POST, 'action') : NULL;
        }
        // recupera la opción de un menú
        else {
            $request = filter_has_var(INPUT_GET, 'option') ? filter_input(INPUT_GET, 'option') : NULL;
        }

        switch ($request) {//lñegan las acciones de menu y los botones 
            case "form_add": 
                $this->formAdd();
                break;
            case "add"://cuando haga click al boton add
                $this->add();
                break;
            case "list_all":
                $this->listAll();
                break;
            case "form_modify":// serach modify i dellete 
                $this->formModify();
                break;
            case "search":// serach modify i dellete 
                $this->searchById();
                break;
            case "modify":// serach modify i dellete 
                $this->modify();
                break;
            case "delete":// serach modify i dellete 
                $this->delete();
                break;
            default:
                $this->view->display();
        }
    }

    // ejecuta la acción de mostrar todas las employees
    public function listAll() {
        $employees = $this->model->listAll();

        if (!empty($employees)) { // array void or array of employee objects?
            $_SESSION['info'] = EmployeeMessage::INF_FORM['found'];
        } else {
            $_SESSION['error'] = EmployeeMessage::ERR_FORM['not_found'];
        }

        $this->view->display("view/form/EmployeeList.php", $employees);
    }

    // carga el formulario de insertar categoría
    public function formAdd() {
        $categories = $this->model->listCategories();
        $this->view->display("view/form/EmployeeFormAdd.php", null, $categories);
       // $this->view->display("view/form/CategoryFormAdd.php");

    }

    // ejecuta la acción de insertar categoría
    public function add() {
        $employeeValid = EmployeeFormValidation::checkData(EmployeeFormValidation::ADD_FIELDS);
        if (empty($_SESSION['error'])) {
            $employee = $this->model->searchById($employeeValid->getId());

            if (is_null($employee)) {
                $result = $this->model->add($employeeValid);

                if ($result == TRUE) {
                    $_SESSION['info'] = EmployeeMessage::INF_FORM['insert'];
                    $employeeValid = NULL;
                }
            } else {
                $_SESSION['error'] = EmployeeMessage::ERR_FORM['exists_id'];
            }
        }
        $categories = $this->model->listCategories();
        $this->view->display("view/form/EmployeeFormAdd.php", $employeeValid, $categories);
    }

    // ejecuta la acción de buscar categoría por id de categoría A PUESTO ALGODE ESTO EN GITLAB 
    public function searchById() {
        $employeeValid = EmployeeFormValidation::checkData(EmployeeFormValidation::SEARCH_FIELDS);

        if (empty($_SESSION['error'])) {
            $employee = $this->model->searchById($employeeValid->getId());

            if (!is_null($employee)) { // is NULL or employee object?
                $_SESSION['info'] = EmployeeMessage::INF_FORM['found'];
                $employeeValid = $employee;
            } else {
                $_SESSION['error'] = EmployeeMessage::ERR_FORM['not_found'];
            }
        }

        $employees = $this->model->listCategories();
        $this->view->display("view/form/EmployeeFormModify.php", $employeeValid,$employees);
    }

    // carga el formulario de modificar categoria
    public function formModify() {
        $categories = $this->model->listCategories();
        $this->view->display("view/form/EmployeeFormModify.php", null, $categories);
    }

    // ejecuta la acción de modificar categoría    
    public function modify() {
         $employeeValid = EmployeeFormValidation::checkData(EmployeeFormValidation::MODIFY_FIELDS);

        if (empty($_SESSION['error'])) {
            $employee = $this->model->searchById($employeeValid->getId());

            if (!is_null($employee)) {
                $result = $this->model->modify($employeeValid);

                if ($result == TRUE) {
                    $_SESSION['info'] = EmployeeMessage::INF_FORM['update'];
                    $EmployeeValid = NULL;
                }
            } else {
                $_SESSION['error'] = EmployeeMessage::ERR_FORM['not_exists_id'];
            }
        }
        $employees = $this->model->listCategories();
        $this->view->display("view/form/EmployeeFormModify.php", $employeeValid,$employees);
       
    }

    // ejecuta la acción de eliminar categoría    
    public function delete() {
          $employeeValid = EmployeeFormValidation::checkData(EmployeeFormValidation::DELETE_FIELDS);

        if (empty($_SESSION['error'])) {
            $employee = $this->model->searchById($employeeValid->getId());

            if (!is_null($employee)) {
                $result = $this->model->delete($employeeValid->getId());

                if ($result == TRUE) {
                    $_SESSION['info'] = EmployeeMessage::INF_FORM['delete'];
                    $employeeValid = NULL;//para que me muestre vacia la lista
                }
            } else {
                $_SESSION['error'] = EmployeeMessage::ERR_FORM['not_exists_id'];
            }
        }
        $employees = $this->model->listCategories();
        $this->view->display("view/form/EmployeeFormModify.php", $employeeValid,$employees);
    }

    // carga el formulario de buscar productos por nombre de categoría
    public function formListProducts() {
        
    }

    // ejecuta la acción de buscar productos por nombre de categoría
    public function listProducts() {
        
    }

}
