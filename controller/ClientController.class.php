<?php

require_once "controller/ControllerInterface.php";
require_once "view/ClientView.class.php";
require_once "model/ClientModel.class.php";
require_once "model/Client.class.php";
require_once "util/ClientMessage.class.php";
require_once "util/ClientFormValidation.class.php";

class ClientController implements ControllerInterface {

    private $view;
    private $model;

    public function __construct() {
        // carga la vista
        $this->view = new ClientView();
        
        // carga el modelo de datos
        $this->model = new ClientModel();
    }

    // carga la vista según la opción o ejecuta una acción específica
    public function processRequest() {

        $request = NULL;
        $_SESSION['info'] = array();
        $_SESSION['error'] = array();

        // recupera la acción de un formulario
        if (filter_has_var(INPUT_POST, 'action')) {
            $request = filter_has_var(INPUT_POST, 'action') ? filter_input(INPUT_POST, 'action') : NULL;
        }
        // recupera la opción de un menú
        else {
            $request = filter_has_var(INPUT_GET, 'option') ? filter_input(INPUT_GET, 'option') : NULL;
        }

        switch ($request) {//lñegan las acciones de menu y los botones 
            case "form_add": 
                $this->formAdd();
                break;
            case "add"://cuando haga click al boton add
                $this->add();
                break;
            case "list_all":
                $this->listAll();
                break;
            case "form_modify":// serach modify i dellete 
                $this->formModify();
                break;
            case "search":// serach modify i dellete 
                $this->searchById();
                break;
            case "modify":// serach modify i dellete 
                $this->modify();
                break;
            case "delete":// serach modify i dellete 
                $this->delete();
                break;
            default:
                $this->view->display();
        }
    }

    // ejecuta la acción de mostrar todas las categorías
    public function listAll() {
        $clients = $this->model->listAll();

        if (!empty($clients)) { 
            $_SESSION['info'] = ClientMessage::INF_FORM['found'];
        } else {
            $_SESSION['error'] = ClientMessage::ERR_FORM['not_found'];
        }

        $this->view->display("view/form/ClientList.php", $clients);
    }

    // carga el formulario de insertar categoría
    public function formAdd() {
        $products = $this->model->listProducts();
        $this->view->display("view/form/ClientFormAdd.php", null, $products);
    }

    // ejecuta la acción de insertar categoría
    public function add() {
        $clientValid = ClientFormValidation::checkData(ClientFormValidation::ADD_FIELDS);
        if (empty($_SESSION['error'])) {
            $client = $this->model->searchById($clientValid->getId());

            if (is_null($client)) {
                $result = $this->model->add($clientValid);

                if ($result == TRUE) {
                    $_SESSION['info'] = ClientMessage::INF_FORM['insert'];
                    $clientValid = NULL;
                }
            } else {
                $_SESSION['error'] = ClientMessage::ERR_FORM['exists_id'];
            }
        }

        $products = $this->model->listProducts();
       
        $this->view->display("view/form/ClientFormAdd.php", $clientValid, $products);
        
    }

    // ejecuta la acción de buscar categoría por id de categoría A PUESTO ALGODE ESTO EN GITLAB 
    public function searchById() {
        $clientValid = ClientFormValidation::checkData(ClientFormValidation::SEARCH_FIELDS);

        if (empty($_SESSION['error'])) {
            $client = $this->model->searchById($clientValid->getId());

            if (!is_null($client)) { // is NULL or client object?
                $_SESSION['info'] = ClientMessage::INF_FORM['found'];
                $clientValid = $client;
            } else {
                $_SESSION['error'] = ClientMessage::ERR_FORM['not_found'];
            }
        }
        $products = $this->model->listProducts();
        $this->view->display("view/form/ClientFormModify.php", $clientValid, $products);
    }

    // carga el formulario de modificar categoria
    public function formModify() {
        $products = $this->model->listProducts();
        $this->view->display("view/form/ClientFormModify.php", null, $products);
       
    }

    // ejecuta la acción de modificar categoría    
    public function modify() {
         $clientValid = ClientFormValidation::checkData(ClientFormValidation::MODIFY_FIELDS);

        if (empty($_SESSION['error'])) {
            $client = $this->model->searchById($clientValid->getId());

            if (!is_null($client)) {
                $result = $this->model->modify($clientValid);

                if ($result == TRUE) {
                    $_SESSION['info'] = ClientMessage::INF_FORM['update'];
                    $clientValid = NULL;
                }
            } else {
                $_SESSION['error'] = ClientMessage::ERR_FORM['not_exists_id'];
            }
        }
        
        $products = $this->model->listProducts();
        $this->view->display("view/form/ClientFormModify.php", $clientValid, $products);
    }

    // ejecuta la acción de eliminar categoría    
    public function delete() {
          $clientValid = ClientFormValidation::checkData(ClientFormValidation::DELETE_FIELDS);

        if (empty($_SESSION['error'])) {
            $client = $this->model->searchById($clientValid->getId());

            if (!is_null($client)) {
                $result = $this->model->delete($clientValid->getId());

                if ($result == TRUE) {
                    $_SESSION['info'] = ClientMessage::INF_FORM['delete'];
                    $clientValid = NULL;//para que me muestre vacia la lista
                }
            } else {
                $_SESSION['error'] = ClientMessage::ERR_FORM['not_exists_id'];
            }
        }
        
        $products = $this->model->listProducts();
        $this->view->display("view/form/ClientFormModify.php", $clientValid, $products);
       
    }

    // carga el formulario de buscar productos por nombre de categoría
    public function formListProducts() {
        
    }

    // ejecuta la acción de buscar productos por nombre de categoría
    public function listProducts() {
        
    }

}
