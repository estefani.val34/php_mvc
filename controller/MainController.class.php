<?php

require_once "controller/UserController.class.php";
require_once "controller/ClientController.class.php";
require_once "controller/EmployeeController.class.php";
require_once "controller/CategoryController.class.php";
require_once "controller/ProductController.php";

class MainController {

    // carga la vista según la opción
    public function processRequest() {

        //$request=filter_has_var(INPUT_GET, 'menu')?filter_input(INPUT_GET, 'menu'):NULL;

        $request = NULL;
        // recupera la opción de un menú
        if (filter_has_var(INPUT_GET, 'menu')) {
            $request = filter_input(INPUT_GET, 'menu');
        }

        switch ($request) {
            case "user":
                $controlUser = new UserController();
                $controlUser->processRequest();
                break;
            case "client":
                $controlClient = new ClientController();
                $controlClient->processRequest();
                break;

            case "employee":
                $controlEmployee = new EmployeeController();
                $controlEmployee->processRequest();
                break;

            case "product":
                $controlProduct = new ProductController();
                $controlProduct->processRequest();

                break;
            case "category":
                $controlCategory = new CategoryController();
                $controlCategory->processRequest();
                break;

            default:
                $controlCategory = new CategoryController();
                $controlCategory->processRequest();
                break;
        }
    }

}
