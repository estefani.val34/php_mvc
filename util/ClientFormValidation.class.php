<?php

class ClientFormValidation {

    const ADD_FIELDS = array('id', 'name', 'surname', 'product');
    const MODIFY_FIELDS = array('id', 'name','surname', 'product');
    const DELETE_FIELDS = array('id');
    const SEARCH_FIELDS = array('id');
    const NUMERIC = "/[^0-9]/";
    const NUMERIC_PRICE = "/[^0-9|\.]/";
    const ALPHABETIC = "/[^a-z A-Z|-]/";

    public static function checkData($fields) {
        $id = NULL;
        $name = NULL;
        $surname = NULL;
        $product = NULL;


        foreach ($fields as $field) {
            switch ($field) {
                case 'id':
                    // filter_var retorna los datos filtrados o FALSE si el filtro falla
                    $id = trim(filter_input(INPUT_POST, 'id'));
                    // print_r($id);
                    $idValid = !preg_match(self::NUMERIC, $id);
                    if (empty($id)) {
                        array_push($_SESSION['error'], ClientMessage::ERR_FORM['empty_id']);
                    } else if ($idValid == FALSE) {
                        array_push($_SESSION['error'], ClientMessage::ERR_FORM['invalid_id']);
                    }
                    break;
                case 'name':
                    $name = trim(filter_input(INPUT_POST, 'name'));
                    $nameValid = !preg_match(self::ALPHABETIC, $name);
                    if (empty($name)) {
                        array_push($_SESSION['error'], ClientMessage::ERR_FORM['empty_name']);
                    } else if ($nameValid == FALSE) {
                        array_push($_SESSION['error'], ClientMessage::ERR_FORM['invalid_name']);
                    }
                    break;
                case 'surname':
                    $surname= trim(filter_input(INPUT_POST, 'surname'));
                    break;
                case 'product':
                    $product = trim(filter_input(INPUT_POST, 'product'));
                    break;
            }
        }

        $client = new Client($id,$name, $surname, $product);
        return $client;
    }

}
