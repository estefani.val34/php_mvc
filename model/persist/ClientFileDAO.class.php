<?php

require_once "model/ModelInterface.class.php";
require_once "model/persist/ConnectFile.class.php";

class ClientFileDAO implements ModelInterface {

    private static $instance = NULL; // instancia de la clase
    private $connect; // conexión actual

    const FILE = "model/resource/clients.txt";

    public function __construct() {
        $this->connect = new ConnectFile(self::FILE);
    }

    // singleton: patrón de diseño que crea una instancia única
    // para proporcionar un punto global de acceso y controlar
    // el acceso único a los recursos físicos
    public static function getInstance(): ClientFileDAO {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * select all categories from file
     * @param void
     * @return array of client objects or array void
     */
    public function listAll(): array {
        $result = array();

        // abre el fichero en modo read
        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);
                    $client = new Client($fields[0], $fields[1],$fields[2], $fields[3]);
                    array_push($result, $client);
                }
            }
            $this->connect->closeFile();
        }
       // print_r($result);
        return $result;
    }

    /**
     * insert a client in file
     * @param $client Client object to insert
     * @return TRUE or FALSE
     */
    public function add($client): bool {
        $result = FALSE;

        // abre el fichero en modo append
        if ($this->connect->openFile("a+")) {
            fputs($this->connect->getHandle(), $client->__toString());
            $this->connect->closeFile();
            $result = TRUE;
        }

        return $result;
    }

    /**
     * select a client by Id from file
     * @param $id string Client Id
     * @return Client object or NULL
     */
    public function searchById($id) {
        $client = NULL;
        // abre el fichero en modo read
        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);

                    if ($id == $fields[0]) {
                        $client = new Client($fields[0], $fields[1],$fields[2], $fields[3]);
                        break;
                    }
                }
            }
            $this->connect->closeFile();
        }

        return $client;
    }

    /**
     * update a client in file
     * @param $client client object to update
     * @return TRUE or FALSE
     */
    public function modify($client): bool {
        $result = FALSE;
        $fileData = array();

        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);

                    if ($client->getId() == $fields[0]) { 
                        array_push($fileData, $client->__toString()); 
                    } else {
                        array_push($fileData, $line . "\n");
                    }
                }
            }
            $this->connect->closeFile();
        }
        if ($this->connect->writeFile($fileData)) {
            $result = TRUE;
        }
        return $result;
    }

    /**
     * delete a client in file
     * @param $id string client Id to delete
     * @return TRUE or FALSE
     */
    public function delete($id): bool {
        $result = FALSE;
        $fileData = array();

        if ($this->connect->openFile("r")) {
            while (!feof($this->connect->getHandle())) {
                $line = trim(fgets($this->connect->getHandle()));
                if ($line != "") {
                    $fields = explode(";", $line);

                    if ($id !== $fields[0]) {//guardo la line si es distinto 
                         array_push($fileData, $line . "\n");
                    } 
                }
            }
            $this->connect->closeFile();
        }
        if ($this->connect->writeFile($fileData)) {
            $result = TRUE;
        }
        return $result;
    }

}
