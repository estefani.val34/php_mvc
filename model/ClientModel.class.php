<?php

require_once "model/persist/ClientFileDAO.class.php";
require_once "model/ProductModel.class.php";

class ClientModel {

    private $dataClient;

    public function __construct() {
        // File
        $this->dataClient = ClientFileDAO::getInstance();

    }

    /**
     * select all clients
     * @param void
     * @return array of client objects or array void
     */
    public function listAll(): array {
        $clients = $this->dataClient->listAll();
        return $clients;
    }

    /**
     * insert a client
     * @param $client Client object to insert
     * @return TRUE or FALSE
     */
    public function add($client): bool {
        $result = $this->dataClient->add($client);

        if ($result == FALSE) {
            $_SESSION['error'] = ClientMessage::ERR_DAO['insert'];
        }
        return $result;
    }

    /**
     * select a client by Id
     * @param $id string client Id
     * @return client object or NULL
     */
    public function searchById($id) {
        $client = $this->dataClient->searchById($id);
        return $client;
    }

    /**
     * update a client
     * @param $client client object to update
     * @return TRUE or FALSE
     */
    public function modify($client): bool {
        $result = $this->dataClient->modify($client);
        if ($result == FALSE ) {//false
            $_SESSION['error'] = ClientMessage::ERR_DAO['update'];
        }
        return $result;
    }

    /**
     * delete a client . I can delete a client if this have products , this is in used
     * @param $id string client Id to delete
     * @return TRUE or FALSE
     * 
     */
    public function delete($id): bool {
        $result = $this->dataClient->delete($id);
        if (!$result) {
            $_SESSION['error'] = ClientMessage::ERR_DAO['delete'];
        }
        return $result;
    }
    
    public function listProducts(): array {
        $productModel = new ProductModel();
        return $productModel->listAll();
    }
}
